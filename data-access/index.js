module.exports = configurations => ({
	...require('./deleteDog')(configurations),
	...require('./editDog')(configurations),
	...require('./getDog')(configurations),
	...require('./getDogs')(configurations),
	...require('./mongoose')(configurations),
	...require('./postDog')(configurations),
})
const Dogs = require("../mongoose/models/dogs")
var ObjectId = require('mongoose').Types.ObjectId

const getSet = (name, breed, age) =>{
	let $set = {
			name: name,
			breed: breed,
			age: age
	}
	let values = Object.values($set)
	let keys = Object.keys($set)
	let i = 0
	for (i; i<values.length; i++){
			let prop = values[i]
			if (prop == undefined){
					let key = keys[i]
					delete $set[key]
			}
	}
	let set = {
			$set
	}
	return set 
}

const editDog = ({}) => async ({id, name, breed, age}) => {
	try{
		let isValidId = ObjectId.isValid(id)
		if (!isValidId) return -1
		let dog = await Dogs.findById({_id: id})
		if (dog == null) return false
		let set = getSet(name, breed, age)
		let editedDog = await Dogs.findOneAndUpdate({_id: id},set, {new: true})
		return editedDog
	}catch (error) {
		throw error
	}
}

module.exports = configurations => ({
	editDog: editDog(configurations)
})
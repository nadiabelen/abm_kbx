module.exports = ({mongoose}) => {
    require('./connection')(mongoose)
    let models = require('./models')
    const dals = require('./dals')(models)
    return dals
}

const mongoose = require('mongoose');

var Schema = new mongoose.Schema({
    name: String,
    breed: String,
    age: Number
})

module.exports = mongoose.model('Dogs', Schema)


const Dogs = require("../mongoose/models/dogs")
var ObjectId = require('mongoose').Types.ObjectId

const deleteDog = ({}) => async ({id}) => {
	try {
			let isValidId = ObjectId.isValid(id)
			if (isValidId == false) return -1
			let dog = await Dogs.findById({_id: id})
			if (dog == null) return false
			let deletedDog = await Dogs.findByIdAndDelete({_id: id})
			if (deletedDog._id == id){
				return true
			}else{
				return false
			}
	}catch (error) {
			throw error
	}
}

module.exports = configurations => ({
	deleteDog: deleteDog(configurations)
})
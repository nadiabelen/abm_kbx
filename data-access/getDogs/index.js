const Dogs = require("../mongoose/models/dogs")

const getDogs = ({}) => async ({}) => {
	try {
		let dogs = await Dogs.find({})
		return dogs
	}catch (error) {
		throw error
	}
}

module.exports = configurations => ({
	getDogs: getDogs(configurations)
})
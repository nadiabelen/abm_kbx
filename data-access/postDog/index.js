const Dogs = require("../mongoose/models/dogs")

const postDog = ({}) => async ({name, breed, age}) => {
	try {
		const dog = new Dogs({name: name, breed: breed, age: age})
		const dogDoc = await dog.save()
		return dogDoc
	}catch (error) {
		throw error
	}
}

module.exports = configurations => ({
	postDog: postDog(configurations)
})
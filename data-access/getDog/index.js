const Dogs = require("../mongoose/models/dogs")
var ObjectId = require('mongoose').Types.ObjectId

const getDog = ({}) => async ({id}) => {
	try {
		let isValidId = ObjectId.isValid(id)
		if (!isValidId) return -1
		let dog = await Dogs.findById({_id: id})
		return dog
	}catch (error) {
		throw error
	}
}

module.exports = configurations => ({
	getDog: getDog(configurations)
})
const { DeleteDogError, WrongIdError, NotExistingDog} = require("./errors");

exports.deleteDog = ({deleteDog}) => async ({id}) => {
    try{
        let isDogDeleted = await deleteDog({id})
        if (isDogDeleted == -1) throw new WrongIdError()
        if(!isDogDeleted) throw new NotExistingDog()
        return 'Dog successfully deleted'
    }catch(error){
        throw new DeleteDogError(error)
    }
    
}
const { deleteDog } = require("./deleteDog.uoc");
const actor = require("../common/actor");
module.exports = (dependencies) => actor(dependencies)(deleteDog(dependencies))
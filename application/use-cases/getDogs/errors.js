class getDogsError extends Error {
    constructor(details, ...args) {
        super(...args)
        this.code = "GR001"
        this.message = "getDogs error"
        this.details = details
        Error.captureStackTrace(this, getDogsError)
    }
}

module.exports = {getDogsError}
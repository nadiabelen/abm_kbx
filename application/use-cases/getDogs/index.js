const { getDogs } = require("./getdogs.uoc");
const actor = require("../common/actor");
module.exports = (dependencies) => actor(dependencies)(getDogs(dependencies))
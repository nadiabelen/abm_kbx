const { GetDogError, WrongIdError, NotExistingDog } = require("./errors")

exports.getDog = ({getDog}) => async ({id}) => {
    try{
        let dog = await getDog({id})
        if (dog == -1) throw new WrongIdError()
        if(dog == null) throw new NotExistingDog()
        return dog
    }catch(error){
        throw new GetDogError(error)
    }
    
}
class GetDogError extends Error {
    constructor(details, ...args) {
        super(...args)
        this.code = "GD"
        this.message = "GetDog error"
        this.details = details
        Error.captureStackTrace(this, GetDogError)
    }
}
class WrongIdError extends Error {
    constructor(details, ...args) {
        super(...args)
        this.code = "WI"
        this.message = "Wrong id"
        this.details = details
        Error.captureStackTrace(this, WrongIdError)
    }
}
class NotExistingDog extends Error {
    constructor(details, ...args) {
        super(...args)
        this.code = "NED"
        this.message = "Dog does not exist"
        this.details = details
        Error.captureStackTrace(this, NotExistingDog)
    }
}

module.exports = {GetDogError, WrongIdError, NotExistingDog}
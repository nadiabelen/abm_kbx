const { getDog } = require("./getDog.uoc");
const actor = require("../common/actor");
module.exports = (dependencies) => actor(dependencies)(getDog(dependencies))
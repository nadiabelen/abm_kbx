const deleteDogUOC = require('./deleteDog')
const editDogUOC = require('./editDog')
const getDogUOC = require('./getDog')
const getDogsUOC = require('./getDogs')
const postDogUOC = require('./postDog')

module.exports = (dependencies) => ({
		deleteDogUOC: deleteDogUOC(dependencies),
	editDogUOC: editDogUOC(dependencies),
	getDogUOC: getDogUOC(dependencies),
	getDogsUOC: getDogsUOC(dependencies),
	postDogUOC: postDogUOC(dependencies),

})
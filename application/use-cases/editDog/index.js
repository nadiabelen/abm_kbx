const { editDog } = require("./editDog.uoc");
const actor = require("../common/actor");
module.exports = (dependencies) => actor(dependencies)(editDog(dependencies))
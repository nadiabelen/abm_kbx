class EditDogError extends Error {
    constructor(details, ...args) {
        super(...args)
        this.code = "EDE"
        this.message = "EditDog error"
        this.details = details
        Error.captureStackTrace(this, EditDogError)
    }
}
class WrongIdError extends Error {
    constructor(details, ...args) {
        super(...args)
        this.code = "WI"
        this.message = "Wrong ID"
        this.details = details
        Error.captureStackTrace(this, WrongIdError)
    }
}
class NotExistingDog extends Error {
    constructor(details, ...args) {
        super(...args)
        this.code = "NED"
        this.message = "Dog does not exist"
        this.details = details
        Error.captureStackTrace(this, NotExistingDog)
    }
}

module.exports = {EditDogError, WrongIdError, NotExistingDog}
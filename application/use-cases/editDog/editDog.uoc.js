const { EditDogError, WrongIdError, NotExistingDog} = require("./errors");

exports.editDog = ({editDog}) => async ({id, name, breed, age}) => {
    try{
        let isDogEdited = await editDog({id, name, breed, age})
        if (isDogEdited == -1) throw new WrongIdError()
        if (!isDogEdited)  throw new NotExistingDog()
        return isDogEdited
    }catch(error){
        throw new EditDogError(error)
    }
    
}
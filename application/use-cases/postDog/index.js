const { postDog } = require("./postDog.uoc");
const actor = require("../common/actor");
module.exports = (dependencies) => actor(dependencies)(postDog(dependencies))
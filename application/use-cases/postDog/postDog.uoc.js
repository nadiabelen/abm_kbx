const { PostDogError } = require("./errors");

exports.postDog = ({postDog}) => async ({name, breed, age}) => {
    try{
        let isDogCreated = await postDog({name, breed, age})
        return isDogCreated
    }catch(error){
        throw new PostDogError(error)
    }
}
class EditDogError extends Error {
    constructor(details, ...args) {
        super(...args)
        this.code = "ED001"
        this.message = "EditDog error"
        this.details = details
        Error.captureStackTrace(this, EditDogError)
    }
}

module.exports = {EditDogError}
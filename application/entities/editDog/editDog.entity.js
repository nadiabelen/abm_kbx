const { EditDogError } = require("./errors")

exports.editDog = ({editDog}) => async ({id, name, breed, age}) => {
    try{
        let editedDog = await editDog({id, name, breed, age})
        return editedDog
    }catch(error){
        throw new EditDogError(error)
    }   
}
const { getDogsError } = require("./errors")

exports.getDogs = ({getDogs}) => async ({}) => {
    try{
        let dogs = await getDogs({})
        return dogs
    }catch(error){
        throw new getDogsError(error)
    } 
}
class DeleteDogError extends Error {
    constructor(details, ...args) {
        super(...args)
        this.code = "DD001"
        this.message = "DeleteDog error"
        this.details = details
        Error.captureStackTrace(this, DeleteDogError)
    }
}

module.exports = {DeleteDogError}
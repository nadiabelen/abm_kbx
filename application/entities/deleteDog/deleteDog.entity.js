const { DeleteDogError } = require("./errors");

exports.deleteDog = ({deleteDog}) => async ({id}) => {
    try{
        const isDogDeleted = await deleteDog({id})
        return isDogDeleted
    }catch(error){
        throw new DeleteDogError(error)
    }
}
class GetDogError extends Error {
    constructor(details, ...args) {
        super(...args)
        this.code = "GD001"
        this.message = "GetDog error"
        this.details = details
        Error.captureStackTrace(this, GetDogError)
    }
}

module.exports = {GetDogError}
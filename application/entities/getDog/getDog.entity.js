const { GetDogError } = require("./errors")

exports.getDog = ({getDog}) => async ({id}) => {
    try{
        let dog = await getDog({id})
        return dog
    }catch(error){
        throw new GetDogError(error)
    }
    
}
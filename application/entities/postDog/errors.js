class PostDogError extends Error {
    constructor(details, ...args) {
        super(...args)
        this.code = "PD001"
        this.message = "PostDog error"
        this.details = details
        Error.captureStackTrace(this, PostDogError)
    }
}

module.exports = {PostDogError}
const { PostDogError } = require("./errors")

exports.postDog = ({postDog}) => async ({name, breed, age}) => {
    try{
        let createdDog = await postDog({name, breed, age})
        return createdDog
    }catch(error){
        throw new PostDogError(error)
    }
}
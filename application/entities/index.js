const deleteDogEntity = require('./deleteDog')
const editDogEntity = require('./editDog')
const getDogEntity = require('./getDog')
const getDogsEntity = require('./getDogs')
const postDogEntity = require('./postDog')

module.exports = (dependencies) => ({
	deleteDog: deleteDogEntity(dependencies),
	editDog: editDogEntity(dependencies),
	getDog: getDogEntity(dependencies),
	getDogs: getDogsEntity(dependencies),
	postDog: postDogEntity(dependencies),

})
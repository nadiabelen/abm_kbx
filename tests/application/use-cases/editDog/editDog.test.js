const {validTrue, validFalse, errored, assertable, responder} = require("../../../common/helpers");
const { EditDogError } = require("../../../../application/use-cases/editDog/errors");
const editDogUOC = require("../../../../application/use-cases/editDog");

//Actor dependencies: this code does not need to be considered in the tests (asume the correct actor access the uoc)
const commonDependencies = {getUserAndAppByIdentification: validTrue,can: validTrue}

describe("editDog uoc test", () => {
    test("should be implemented", async () => {
        let dependencies = {
            //complete with uoc dependencies
        }
        Object.assign(dependencies,commonDependencies)
        let uoc = editDogUOC(dependencies)
        await uoc({/*complete with uoc parameters*/})
        
    });
    
})
const {validTrue, validFalse, errored, assertable, responder} = require("../../../common/helpers");
const { PostDogError } = require("../../../../application/use-cases/postDog/errors");
const postDogUOC = require("../../../../application/use-cases/postDog");

//Actor dependencies: this code does not need to be considered in the tests (asume the correct actor access the uoc)
const commonDependencies = {getUserAndAppByIdentification: validTrue,can: validTrue}

describe("postDog uoc test", () => {
    test("should be implemented", async () => {
        let dependencies = {
            //complete with uoc dependencies
        }
        Object.assign(dependencies,commonDependencies)
        let uoc = postDogUOC(dependencies)
        await uoc({/*complete with uoc parameters*/})
        
    });
    
})
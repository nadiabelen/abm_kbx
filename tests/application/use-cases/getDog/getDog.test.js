const {validTrue, validFalse, errored, assertable, responder} = require("../../../common/helpers");
const { GetDogError } = require("../../../../application/use-cases/getDog/errors");
const getDogUOC = require("../../../../application/use-cases/getDog");

//Actor dependencies: this code does not need to be considered in the tests (asume the correct actor access the uoc)
const commonDependencies = {getUserAndAppByIdentification: validTrue,can: validTrue}

describe("getDog uoc test", () => {
    test("should be implemented", async () => {
        let dependencies = {
            //complete with uoc dependencies
        }
        Object.assign(dependencies,commonDependencies)
        let uoc = getDogUOC(dependencies)
        await uoc({/*complete with uoc parameters*/})
        
    });
    
})
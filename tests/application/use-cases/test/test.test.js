const {validTrue, validFalse, errored, assertable, responder} = require("../../../common/helpers");
const { TestError } = require("../../../../application/use-cases/test/errors");
const testUOC = require("../../../../application/use-cases/test");

//Actor dependencies: this code does not need to be considered in the tests (asume the correct actor access the uoc)
const commonDependencies = {getUserAndAppByIdentification: validTrue,can: validTrue}

describe("test uoc test", () => {
    test("should be implemented", async () => {
        let dependencies = {
            //complete with uoc dependencies
        }
        Object.assign(dependencies,commonDependencies)
        let uoc = testUOC(dependencies)
        await uoc({/*complete with uoc parameters*/})
        
    });
    
})
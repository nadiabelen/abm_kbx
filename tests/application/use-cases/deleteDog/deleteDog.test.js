const {validTrue, validFalse, errored, assertable, responder} = require("../../../common/helpers");
const { DeleteDogError } = require("../../../../application/use-cases/deleteDog/errors");
const deleteDogUOC = require("../../../../application/use-cases/deleteDog");

//Actor dependencies: this code does not need to be considered in the tests (asume the correct actor access the uoc)
const commonDependencies = {getUserAndAppByIdentification: validTrue,can: validTrue}

describe("deleteDog uoc test", () => {
    test("should be implemented", async () => {
        let dependencies = {
            //complete with uoc dependencies
        }
        Object.assign(dependencies,commonDependencies)
        let uoc = deleteDogUOC(dependencies)
        await uoc({/*complete with uoc parameters*/})
        
    });
    
})
const {validTrue, validFalse, errored, assertable, responder} = require("../../../common/helpers");
const { getdogsError } = require("../../../../application/use-cases/getdogs/errors");
const getdogsUOC = require("../../../../application/use-cases/getdogs");

//Actor dependencies: this code does not need to be considered in the tests (asume the correct actor access the uoc)
const commonDependencies = {getUserAndAppByIdentification: validTrue,can: validTrue}

describe("getdogs uoc test", () => {
    test("should be implemented", async () => {
        let dependencies = {
            //complete with uoc dependencies
        }
        Object.assign(dependencies,commonDependencies)
        let uoc = getdogsUOC(dependencies)
        await uoc({/*complete with uoc parameters*/})
        
    });
    
})
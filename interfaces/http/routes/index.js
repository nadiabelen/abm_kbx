module.exports = ({getDogsUOC, postDogUOC, getDogUOC, deleteDogUOC, editDogUOC}) => ([
    {path:'/dogs', verb:'GET',uoc:getDogsUOC},
    {path:'/dogs', verb:'POST', uoc:postDogUOC},
    {path:'/dogs/:id', verb:'GET', uoc:getDogUOC},
    {path:'/dogs/:id', verb:'DELETE', uoc:deleteDogUOC},
    {path:'/dogs/:id', verb:'PUT', uoc:editDogUOC}
])